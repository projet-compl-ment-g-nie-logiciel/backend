# Projet Complément Génie Logiciel (Backend)
Partie backend du projet de Complément Génie Logiciel, développé avec Spring, swagger à disposition (en ajoutant /swagger.html à l'URL)
## Exécution en local
En phase de développement, excécuter la classe main de l'application :
```
com.tt.projetcmplgenlog.ProjetCmplGenLogApplication.java
```
Accès par  l'adresse http://localhost:8081/.

## Build
Pour obtenir un build du projet utilisable en production, exécuter cette commande :
```
mvn package
```
Build disponible dans le dossier `target/`

## Application conteneurisée (Docker)
- Bien penser à build le projet avant ! (Voir point précédent).
- Il faut ensuite build une image docker, pour cela exécuter la commande suivante (qui va se baser sur le Dockerfile) :
```
docker build -t ossacipe/projetcomplementgenielogiciel-backend .
```
- Une fois l'image générée, on peut l'exécuter avec la commande suivante :
```
docker run -it -p 8081:8081 --rm --name projetcomplementgenielogiciel-backend ossacipe/projetcomplementgenielogiciel-backend
```
- Accès par l'URL http://localhost:8081/.

## DockerHub
[Lien vers l'image stockée sur DockerHub](https://hub.docker.com/r/ossacipe/projetcomplementgenielogiciel-backend).
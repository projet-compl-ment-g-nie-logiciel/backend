package com.tt.projetcmplgenlog.util;

public class DocumentForm {

        private String name;
        private String path;
        private Long idTypeDocument;

    public DocumentForm() {
    }

    public DocumentForm(String name, String path, Long idTypeDocument) {
        this.name = name;
        this.path = path;
        this.idTypeDocument = idTypeDocument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(Long idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }
}

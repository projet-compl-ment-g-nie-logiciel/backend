package com.tt.projetcmplgenlog.util;

import com.tt.projetcmplgenlog.entity.TypeDocument;

import java.time.LocalDate;
import java.util.Objects;

public class DocumentStats {
    private LocalDate date;
    private TypeDocument typeDocument;
    private Long nbDocuments;

    public DocumentStats(LocalDate date, TypeDocument typeDocument, Long nbDocuments) {
        this.date = date;
        this.typeDocument = typeDocument;
        this.nbDocuments = nbDocuments;
    }

    public DocumentStats(TypeDocument typeDocument, Long nbDocuments) {
        this.typeDocument = typeDocument;
        this.nbDocuments = nbDocuments;
    }

    public DocumentStats(LocalDate date, Long nbDocuments) {
        this.date = date;
        this.nbDocuments = nbDocuments;
    }

    public DocumentStats() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public TypeDocument getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }

    public Long getNbDocuments() {
        return nbDocuments;
    }

    public void setNbDocuments(Long nbDocuments) {
        this.nbDocuments = nbDocuments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentStats that = (DocumentStats) o;
        return Objects.equals(date, that.date) && Objects.equals(typeDocument, that.typeDocument) && Objects.equals(nbDocuments, that.nbDocuments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, typeDocument, nbDocuments);
    }

    @Override
    public String toString() {
        return "DocumentStats{" +
                "date=" + date +
                ", typeDocument=" + typeDocument +
                ", nbDocuments=" + nbDocuments +
                '}';
    }
}

package com.tt.projetcmplgenlog.controller;

import com.tt.projetcmplgenlog.entity.Document;
import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.service.DocumentService;
import com.tt.projetcmplgenlog.service.TypeDocumentService;
import com.tt.projetcmplgenlog.util.DocumentForm;
import com.tt.projetcmplgenlog.util.DocumentStats;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/document")
public class DocumentController {

    private final DocumentService documentService;
    private final TypeDocumentService typeDocumentService;

    public DocumentController(DocumentService documentService, TypeDocumentService typeDocumentService) {
        this.documentService = documentService;
        this.typeDocumentService = typeDocumentService;
    }

    @Operation(summary = "Liste de tous les documents")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "204", description = "no content")
    })
    @GetMapping(value = "/")
    public Page<Document> getAll(@RequestParam Integer pageNumber, @RequestParam Integer pageSize){
        return documentService.getAll(PageRequest.of(pageNumber, pageSize));
    }

    @Operation(summary = "Recupère un document (ou plusieurs) grâce à son nom")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "204", description = "no content")
    })
    @GetMapping(value = "/name/{name}")
    public Page<Document> getByName(@PathVariable("name") String name, @RequestParam Integer pageNumber, @RequestParam Integer pageSize){
        return documentService.getByName(name, PageRequest.of(pageNumber, pageSize));
    }

    @Operation(summary = "Recupère tous les documents du même type")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "204", description = "no content")
    })
    @GetMapping(value = "/type/{idType}")
    public ResponseEntity<List<Document>> getByType(@PathVariable("idType") Long idTypeDocument){
        Optional<TypeDocument> typeDocumentOptional = typeDocumentService.getById(idTypeDocument);
        if(typeDocumentOptional.isPresent()){
            return new ResponseEntity<>(documentService.getByType(typeDocumentOptional.get()), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Création d'un document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Document.class))),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    @PostMapping(value = "/")
    public ResponseEntity<Document> create(@RequestBody DocumentForm documentForm){
        Optional<TypeDocument> typeDocumentOptional = typeDocumentService.getById(documentForm.getIdTypeDocument());
        if(typeDocumentOptional.isPresent()){
            return new ResponseEntity<>(documentService.create(documentForm, typeDocumentOptional.get()), HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Suppression d'un Document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "404", description = "Document not found") })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Document> delete(@PathVariable("id") Long id){
        Optional<Document> documentOptional = documentService.getById(id);
        if(documentOptional.isPresent()){
            documentService.delete(documentOptional.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Stats")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "400", description = "bad parameter")})
    @GetMapping(value = "/stats")
    public ResponseEntity<List<DocumentStats>> getStats(@RequestParam Optional<String> criteria){
        if(criteria.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else if("type".equals(criteria.get())){
            return new ResponseEntity<>(documentService.getStatsByType(), HttpStatus.OK);
        }
        else if("date".equals(criteria.get())){
            return new ResponseEntity<>(documentService.getStatsByDate(), HttpStatus.OK);
        }
        else if("dateAndType".equals(criteria.get())){
            return new ResponseEntity<>(documentService.getStatsByDateAndType(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
package com.tt.projetcmplgenlog.controller;

import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.DocumentRepository;
import com.tt.projetcmplgenlog.repository.TypeDocumentRepository;
import com.tt.projetcmplgenlog.service.DocumentService;
import com.tt.projetcmplgenlog.service.TypeDocumentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/typeDocument")
public class TypeDocumentController {


    private final TypeDocumentService typeDocumentService;

    public TypeDocumentController(TypeDocumentService typeDocumentService) {
        this.typeDocumentService = typeDocumentService;
    }

    @Operation(summary = "Liste de tous les types")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "204", description = "no content")
    })
    @GetMapping(value = "/")
    public List<TypeDocument> getAll(){
        return typeDocumentService.getAll();
    }
}
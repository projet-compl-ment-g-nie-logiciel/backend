package com.tt.projetcmplgenlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetCmplGenLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetCmplGenLogApplication.class, args);
    }

}

package com.tt.projetcmplgenlog.entity;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Table(name="DOCUMENT")
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID", nullable=false)
    private Long id;

    @Column(name="PATH", length=100, nullable=false)
    private String path;

    @Column(name="NAME", length=12, nullable=false)
    private String name;

    @ManyToOne
    @JoinColumn(name="TYPE_ID", nullable=false)
    private TypeDocument typeDocument;

    @Column(name="DATE", nullable=false)
    private LocalDate dateArchivage;

    public Document(String path, String name, TypeDocument typeDocument, LocalDate dateArchivage) {
        this.path = path;
        this.name = name;
        this.typeDocument = typeDocument;
        this.dateArchivage = dateArchivage;
    }

    public Long getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeDocument getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }

    public LocalDate getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(LocalDate dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return Objects.equals(id, document.id) && Objects.equals(path, document.path) && Objects.equals(name, document.name) && Objects.equals(typeDocument, document.typeDocument) && Objects.equals(dateArchivage, document.dateArchivage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, path, name, typeDocument, dateArchivage);
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", name='" + name + '\'' +
                ", typeDocument=" + typeDocument +
                ", dateArchivage=" + dateArchivage +
                '}';
    }
}
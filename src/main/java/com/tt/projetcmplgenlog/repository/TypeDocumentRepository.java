package com.tt.projetcmplgenlog.repository;

import com.tt.projetcmplgenlog.entity.TypeDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeDocumentRepository extends JpaRepository<TypeDocument,Long> {
}
package com.tt.projetcmplgenlog.repository;

import com.tt.projetcmplgenlog.entity.Document;
import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.util.DocumentStats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DocumentRepository extends JpaRepository<Document,Long>, PagingAndSortingRepository<Document,Long> {
    Page<Document> findByNameIgnoreCaseContains(String name, Pageable pageable);

    List<Document> findByTypeDocument(TypeDocument typeDocument);

    @Query("SELECT new com.tt.projetcmplgenlog.util.DocumentStats(c.dateArchivage, c.typeDocument, COUNT(c)) "
            + "FROM Document AS c GROUP BY c.dateArchivage, c.typeDocument")
    List<DocumentStats> countTotalDocumentByDateAndType();

    @Query("SELECT new com.tt.projetcmplgenlog.util.DocumentStats(c.dateArchivage, COUNT(c)) "
            + "FROM Document AS c GROUP BY c.dateArchivage")
    List<DocumentStats> countTotalDocumentByDate();

    @Query("SELECT new com.tt.projetcmplgenlog.util.DocumentStats(c.typeDocument, COUNT(c)) "
            + "FROM Document AS c GROUP BY c.typeDocument")
    List<DocumentStats> countTotalDocumentByType();
}
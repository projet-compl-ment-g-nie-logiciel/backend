package com.tt.projetcmplgenlog;

import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.TypeDocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Seeder {

    private static final Logger log = LoggerFactory.getLogger(Seeder.class);

    @Bean
    // Seeding de la table TYPE_DOCUMENT
    public CommandLineRunner seedingTypeDocument(TypeDocumentRepository typeDocumentRepository) {
        return args -> {
            log.info("Début du seeding de la table TYPE_DOCUMENT");
            typeDocumentRepository.save(new TypeDocument("Texte"));
            typeDocumentRepository.save(new TypeDocument("Audio"));
            typeDocumentRepository.save(new TypeDocument("Vidéo"));
            typeDocumentRepository.save(new TypeDocument("Binaire"));
            log.info("Fin du seeding de la table TYPE_DOCUMENT");
        };
    }

}
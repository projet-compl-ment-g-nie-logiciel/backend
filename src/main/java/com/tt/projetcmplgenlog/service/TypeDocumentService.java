package com.tt.projetcmplgenlog.service;

import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.TypeDocumentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TypeDocumentService {

    private final TypeDocumentRepository typeDocumentRepository;

    public TypeDocumentService(TypeDocumentRepository typeDocumentRepository) {
        this.typeDocumentRepository = typeDocumentRepository;
    }

    /**
     * Récupére l'ensemble des TypeDocument
     * @return la liste de tous les types
     */
    public List<TypeDocument> getAll(){
        return typeDocumentRepository.findAll();
    }

    /**
     * Recherche un TypeDocument par son ID
     * @param id l'id recherché
     * @return le type ayant l'id spécifié
     */
    public Optional<TypeDocument> getById(Long id){
        return typeDocumentRepository.findById(id);
    }
}

package com.tt.projetcmplgenlog.service;

import com.tt.projetcmplgenlog.entity.Document;
import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.DocumentRepository;
import com.tt.projetcmplgenlog.util.DocumentForm;
import com.tt.projetcmplgenlog.util.DocumentStats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentService {

    private final DocumentRepository documentRepository;

    public DocumentService(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    /**
     * Récupére l'ensemble des Documents
     * @return la liste de tous les documents
     */
    public Page<Document> getAll(Pageable pageable){
        return documentRepository.findAll(pageable);
    }

    /**
     * Recherche un Document par son ID
     * @param id l'id recherché
     * @return le document ayant l'id spécifié
     */
    public Optional<Document> getById(Long id){
        return documentRepository.findById(id);
    }

    /**
     * Recherche des Documents à partir d'un nom
     * @param name le nom recherché
     * @return la liste des documents contenant le nom spécifié
     */
    public Page<Document> getByName(String name, Pageable pageable){
        return documentRepository.findByNameIgnoreCaseContains(name, pageable);
    }

    /**
     * Recherche des Documents à partir d'un type
     * @param type le type recherché
     * @return la liste des documents ayant le type spécifié
     */
    public List<Document> getByType(TypeDocument type){
        return documentRepository.findByTypeDocument(type);
    }

    /**
     * Fonction pour copier l'objet issu du request body dans un document
     * @param documentForm les données reçues
     * @param document le document à compléter
     * @param typeDocument le type voulu
     * @return le document complété
     */
    public Document copyDocumentFormIntoDocumentWithTypeDocument(DocumentForm documentForm, Document document, TypeDocument typeDocument){
        document.setPath(documentForm.getPath());
        document.setName(documentForm.getName());
        document.setTypeDocument(typeDocument);
        document.setDateArchivage(LocalDate.now());
        return document;
    }

    /**
     * Création d'un nouveau Document
     * @param documentForm les données reçues
     * @param typeDocument le type voulu
     * @return le document crée
     */
    public Document create(DocumentForm documentForm, TypeDocument typeDocument){
        return documentRepository.save(copyDocumentFormIntoDocumentWithTypeDocument(documentForm, new Document(), typeDocument));
    }

    /**
     * Supprime un Document
     * @param document le document à supprimer
     */
    public void delete(Document document){
        documentRepository.delete(document);
    }

    /**
     * Récupére les stats par type et date
     * @return les stats par type et par date
     */
    public List<DocumentStats> getStatsByDateAndType(){
        return documentRepository.countTotalDocumentByDateAndType();
    }

    /**
     * Récupére les stats par date
     * @return les stats par date
     */
    public List<DocumentStats> getStatsByDate(){
        return documentRepository.countTotalDocumentByDate();
    }

    /**
     * Récupére les stats par type
     * @return les stats par type
     */
    public List<DocumentStats> getStatsByType(){
        return documentRepository.countTotalDocumentByType();
    }

}
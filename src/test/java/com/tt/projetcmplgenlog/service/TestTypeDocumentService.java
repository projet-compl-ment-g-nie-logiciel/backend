package com.tt.projetcmplgenlog.service;

import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.TypeDocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TestTypeDocumentService {

    @InjectMocks
    TypeDocumentService typeDocumentService;

    @Mock
    private TypeDocumentRepository typeDocumentRepository;

    @Test
    void getAll(){
        TypeDocument type1 = new TypeDocument("Vidéo");
        TypeDocument type2 = new TypeDocument("Image");

        when(typeDocumentRepository.findAll()).thenReturn(Arrays.asList(type1, type2));

        List<TypeDocument> result = typeDocumentService.getAll();

        assertEquals(type1.getLibelle(), result.get(0).getLibelle());
        assertEquals(type2.getLibelle(), result.get(1).getLibelle());
    }

    @Test
    void getById(){

        TypeDocument type1 = new TypeDocument("Vidéo");

        when(typeDocumentRepository.findById(type1.getId())).thenReturn(Optional.of(type1));

        Optional<TypeDocument> result = typeDocumentService.getById(type1.getId());

        assertEquals(Boolean.TRUE, result.isPresent());

    }

}

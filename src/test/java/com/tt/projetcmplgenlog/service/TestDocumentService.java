package com.tt.projetcmplgenlog.service;

import com.tt.projetcmplgenlog.entity.Document;
import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.DocumentRepository;
import com.tt.projetcmplgenlog.util.DocumentForm;
import com.tt.projetcmplgenlog.util.DocumentStats;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TestDocumentService {

    @InjectMocks
    DocumentService documentService;

    @Mock
    private DocumentRepository documentRepository;

    @Test
    void getAll(){

        List<Document> docs = new ArrayList<>();
        Pageable pageable = Pageable.ofSize(1);

        Page<Document> pagedResponse = new PageImpl(docs);
        when(documentRepository.findAll(pageable)).thenReturn(pagedResponse);

        Page<Document> res = documentService.getAll(pageable);

        assertEquals(pagedResponse, res);
    }

    @Test
    void getById(){

        TypeDocument typeDocument1 = new TypeDocument("Image");

        LocalDate date1 = LocalDate.of(2010,10,15);

        Document doc1 = new Document("c:/rapport1.pdf", "Rapport", typeDocument1, date1);

        when(documentRepository.findById(doc1.getId())).thenReturn(Optional.of(doc1));

        Optional<Document> res = documentService.getById(doc1.getId());

        assertEquals(doc1, res.get());
    }

    @Test
    void getByName(){

        String name = "test";

        List<Document> docs = new ArrayList<>();
        Pageable pageable = Pageable.ofSize(1);

        Page<Document> pagedResponse = new PageImpl(docs);
        when(documentRepository.findByNameIgnoreCaseContains(name, pageable)).thenReturn(pagedResponse);

        Page<Document> res = documentService.getByName(name, pageable);

        assertEquals(pagedResponse, res);

    }

    @Test
    void getByType(){

        TypeDocument typeDocument1 = new TypeDocument("Image");

        LocalDate date1 = LocalDate.of(2010,10,15);
        LocalDate date2 = LocalDate.of(2020,11,1);

        Document doc1 = new Document("c:/rapport1.pdf", "Rapport", typeDocument1, date1);
        Document doc2 = new Document("c:/rapport1.pdf", "Rapport2", typeDocument1, date2);

        when(documentRepository.findByTypeDocument(typeDocument1)).thenReturn(Arrays.asList(doc1, doc2));

        List<Document> res = documentService.getByType(typeDocument1);

        assertEquals(doc1, res.get(0));
        assertEquals(doc2, res.get(1));

    }

    @Test
    void copyDocumentFormIntoDocumentWithTypeDocument(){

        String path = "C:/rapport.pdf";
        String name = "Rapport";
        TypeDocument typeDocument1 = new TypeDocument("Texte");
        LocalDate date = LocalDate.now();
        Document doc = new Document(path, name, typeDocument1, date);

        DocumentForm form = new DocumentForm(name, path, typeDocument1.getId());

        Document res = documentService.copyDocumentFormIntoDocumentWithTypeDocument(form,new Document(),typeDocument1);

        assertEquals(doc, res);
    }

    @Test
    void create(){

        String path = "C:/rapport.pdf";
        String name = "Rapport";
        TypeDocument typeDocument1 = new TypeDocument("Texte");
        LocalDate date = LocalDate.now();
        Document doc = new Document(path, name, typeDocument1, date);

        DocumentForm form = new DocumentForm(name, path, typeDocument1.getId());
        when(documentRepository.save(doc)).thenReturn(doc);

        Document res = documentService.create(form,typeDocument1);

        assertEquals(doc, res);
    }

    @Test
    void delete(){

        TypeDocument typeDocument1 = new TypeDocument("Texte");
        Document doc = new Document("C:/rapport.pdf", "Rapport", typeDocument1, LocalDate.now());

        documentService.delete(doc);

        verify(documentRepository).delete(doc);

    }

    @Test
    void getStatsByDateAndType(){

        TypeDocument typeDocument1 = new TypeDocument("Image");
        TypeDocument typeDocument2 = new TypeDocument("Vidéo");

        LocalDate date1 = LocalDate.of(2010,10,15);
        LocalDate date2 = LocalDate.of(2020,11,1);

        DocumentStats documentStats1 = new DocumentStats(date1, typeDocument1, Long.valueOf("15"));
        DocumentStats documentStats2 = new DocumentStats(date2, typeDocument2, Long.valueOf("2"));

        when(documentRepository.countTotalDocumentByDateAndType()).thenReturn(Arrays.asList(documentStats1, documentStats2));

        List<DocumentStats> res = documentService.getStatsByDateAndType();

        assertEquals(documentStats1.getNbDocuments(), res.get(0).getNbDocuments());
        assertEquals(date1, res.get(0).getDate());
        assertEquals(typeDocument1.getLibelle(), res.get(0).getTypeDocument().getLibelle());
        assertEquals(documentStats2.getNbDocuments(), res.get(1).getNbDocuments());
        assertEquals(date2, res.get(1).getDate());
        assertEquals(typeDocument2.getLibelle(), res.get(1).getTypeDocument().getLibelle());

    }

    @Test
    void getStatsByDate(){

        LocalDate date1 = LocalDate.of(2010,10,15);
        LocalDate date2 = LocalDate.of(2020,11,1);

        DocumentStats documentStats1 = new DocumentStats(date1, Long.valueOf("7"));
        DocumentStats documentStats2 = new DocumentStats(date2, Long.valueOf("9"));

        when(documentRepository.countTotalDocumentByDate()).thenReturn(Arrays.asList(documentStats1, documentStats2));

        List<DocumentStats> res = documentService.getStatsByDate();

        assertEquals(documentStats1.getNbDocuments(), res.get(0).getNbDocuments());
        assertEquals(date1, res.get(0).getDate());
        assertEquals(documentStats2.getNbDocuments(), res.get(1).getNbDocuments());
        assertEquals(date2, res.get(1).getDate());

    }

    @Test
    void getStatsByType(){

        TypeDocument typeDocument1 = new TypeDocument("Image");
        TypeDocument typeDocument2 = new TypeDocument("Vidéo");
        DocumentStats documentStats1 = new DocumentStats(typeDocument1, Long.valueOf("7"));
        DocumentStats documentStats2 = new DocumentStats(typeDocument2, Long.valueOf("9"));

        when(documentRepository.countTotalDocumentByType()).thenReturn(Arrays.asList(documentStats1, documentStats2));

        List<DocumentStats> res = documentService.getStatsByType();

        assertEquals(documentStats1.getNbDocuments(), res.get(0).getNbDocuments());
        assertEquals(typeDocument1.getLibelle(), res.get(0).getTypeDocument().getLibelle());
        assertEquals(documentStats2.getNbDocuments(), res.get(1).getNbDocuments());
        assertEquals(typeDocument2.getLibelle(), res.get(1).getTypeDocument().getLibelle());

    }

}

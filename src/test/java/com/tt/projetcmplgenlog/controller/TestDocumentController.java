package com.tt.projetcmplgenlog.controller;

import com.tt.projetcmplgenlog.entity.Document;
import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.repository.DocumentRepository;
import com.tt.projetcmplgenlog.service.DocumentService;
import com.tt.projetcmplgenlog.service.TypeDocumentService;
import com.tt.projetcmplgenlog.util.DocumentForm;
import com.tt.projetcmplgenlog.util.DocumentStats;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TestDocumentController {

    @InjectMocks
    DocumentController documentController;

    @Mock
    private DocumentService documentService;

    @Mock
    private TypeDocumentService typeDocumentService;

    @Mock
    private DocumentRepository documentRepository;

    /**
     * public Page<Document> getAll(@RequestParam Integer pageNumber, @RequestParam Integer pageSize){
     *         return documentService.getAll(PageRequest.of(pageNumber, pageSize));
     *     }
     */
    @Test
    void getAll(){
        List<Document> docs = new ArrayList<>();
        int pageNumber = 0;
        int pageSize = 5;
        Pageable pageable =  PageRequest.of(pageNumber, pageSize);

        Page<Document> pagedResponse = new PageImpl(docs);
        when(documentService.getAll(pageable)).thenReturn(pagedResponse);

        Page<Document> res = documentController.getAll(pageNumber ,pageSize );

        assertEquals(pagedResponse, res);
    }

    /**@GetMapping(value = "/name/{name}")
    public Page<Document> getByName(@PathVariable("name") String name, @RequestParam Integer pageNumber, @RequestParam Integer pageSize){
    return documentService.getByName(name, PageRequest.of(pageNumber, pageSize));
    }*/
    @Test
    void getByName(){
        List<Document> docs = new ArrayList<>();
        int pageNumber = 0;
        int pageSize = 5;
        Pageable pageable =  PageRequest.of(pageNumber, pageSize);
        String name="test";

        Page<Document> pagedResponse = new PageImpl(docs);
        when(documentService.getByName(name,pageable)).thenReturn(pagedResponse);

        Page<Document> res = documentController.getByName(name, pageNumber ,pageSize );

        assertEquals(pagedResponse, res);
    }

    @Test
    void getByType_whenNotPresent(){

        Long id = Long.valueOf("0");

        when(typeDocumentService.getById(id)).thenReturn(Optional.empty());

        ResponseEntity<List<Document>> res = documentController.getByType(id);
        assertEquals(HttpStatus.NOT_FOUND, res.getStatusCode());
    }

    @Test
    void getByType_whenPresent(){

        TypeDocument typeDocument =  new TypeDocument("Image");
        Document doc = new Document("MonDoc","c:/doc.pdf", typeDocument, LocalDate.now());

        when(typeDocumentService.getById(typeDocument.getId())).thenReturn(Optional.of(typeDocument));
        when(documentService.getByType(typeDocument)).thenReturn(Arrays.asList(doc));

        ResponseEntity<List<Document>> res = documentController.getByType(typeDocument.getId());

        assertEquals(res.getBody().get(0), doc);
    }

    @Test
    void create_whenNotPresent(){
        DocumentForm documentForm = new DocumentForm("test","c:/mondoc.pdf",Long.valueOf("0"));

        when(typeDocumentService.getById(documentForm.getIdTypeDocument())).thenReturn(Optional.empty());

        ResponseEntity<Document> res = documentController.create(documentForm);
        assertEquals(HttpStatus.NOT_FOUND, res.getStatusCode());
    }

    @Test
    void create_whenPresent(){

        String name = "test";
        String path = "c:/mondoc.pdf";
        LocalDate date = LocalDate.now();

        TypeDocument typeDocument =  new TypeDocument("Image");

        DocumentForm documentForm = new DocumentForm(name,path,typeDocument.getId());

        Document doc = new Document(name,path, typeDocument, date);

        when(typeDocumentService.getById(typeDocument.getId())).thenReturn(Optional.of(typeDocument));
        when(documentService.create(documentForm, typeDocument)).thenReturn(doc);

        ResponseEntity<Document> res = documentController.create(documentForm);

        assertEquals(res.getBody(), doc);
    }

    @Test
    void delete_whenNotPresent(){

        Long id = Long.valueOf("1");

        when(documentService.getById(id)).thenReturn(Optional.empty());

        ResponseEntity<Document> res = documentController.delete(id);
        assertEquals(HttpStatus.NOT_FOUND, res.getStatusCode());

    }

    @Test
    void delete_whenPresent(){
        TypeDocument typeDocument =  new TypeDocument("Image");

        Document doc = new Document("test","c:/mondoc.pdf", typeDocument, LocalDate.now());

        when(documentService.getById(doc.getId())).thenReturn(Optional.of(doc));

        ResponseEntity<Document> res = documentController.delete(doc.getId());
        verify(documentService).delete(doc);
        assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @Test
    void getStats_whenEmpty(){
        Optional<String> op = Optional.empty();
        ResponseEntity<List<DocumentStats>> res = documentController.getStats(op);
        assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
    }

    @Test
    void getStats_whenType(){

        Optional<String> op = Optional.of("type");
        TypeDocument typeDocument =  new TypeDocument("Image");
        DocumentStats docStat = new DocumentStats(typeDocument, Long.valueOf(5));

        when(documentService.getStatsByType()).thenReturn(Arrays.asList(docStat));

        ResponseEntity<List<DocumentStats>> res = documentController.getStats(op);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertEquals(docStat, res.getBody().get(0));
    }

    @Test
    void getStats_whenDate(){

        Optional<String> op = Optional.of("date");
        LocalDate date = LocalDate.now();

        DocumentStats docStat = new DocumentStats(date, Long.valueOf(8));

        when(documentService.getStatsByDate()).thenReturn(Arrays.asList(docStat));

        ResponseEntity<List<DocumentStats>> res = documentController.getStats(op);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertEquals(docStat, res.getBody().get(0));
    }

    @Test
    void getStats_whenDateAndType(){

        Optional<String> op = Optional.of("dateAndType");
        LocalDate date = LocalDate.now();
        TypeDocument typeDocument =  new TypeDocument("Image");

        DocumentStats docStat = new DocumentStats(date, typeDocument, Long.valueOf(8));

        when(documentService.getStatsByDateAndType()).thenReturn(Arrays.asList(docStat));

        ResponseEntity<List<DocumentStats>> res = documentController.getStats(op);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertEquals(docStat, res.getBody().get(0));

    }

    @Test
    void getStats_whenOthers(){
        Optional<String> op = Optional.of("oui");
        ResponseEntity<List<DocumentStats>> res = documentController.getStats(op);
        assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
    }

}

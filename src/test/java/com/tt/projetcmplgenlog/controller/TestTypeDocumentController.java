package com.tt.projetcmplgenlog.controller;

import com.tt.projetcmplgenlog.entity.TypeDocument;
import com.tt.projetcmplgenlog.service.TypeDocumentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TestTypeDocumentController {

    @InjectMocks
    TypeDocumentController typeDocumentController;

    @Mock
    private TypeDocumentService typeDocumentService;

    @Test
    void getAll(){

        TypeDocument type1 = new TypeDocument("Vidéo");
        TypeDocument type2 = new TypeDocument("Image");

        when(typeDocumentService.getAll()).thenReturn(Arrays.asList(type1, type2));

        List<TypeDocument> result = typeDocumentController.getAll();

        assertEquals(type1.getLibelle(), result.get(0).getLibelle());
        assertEquals(type2.getLibelle(), result.get(1).getLibelle());

    }

}
